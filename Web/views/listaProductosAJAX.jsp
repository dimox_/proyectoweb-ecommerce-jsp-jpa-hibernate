<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Listado Productos</title>

<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

</head>
<body>
	<h1>LISTADO DE PRODUCTOS</h1>
	<table id="productosTable_id" class="display order-column dt-body-right">

		<thead>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Categoría</th>
				<th>Fabricante</th>
				<th>Detalle</th>
				<th>Descripción</th>
				<th>Precio</th>
				<th>Stock</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Categoría</th>
				<th>Fabricante</th>
				<th>Detalle</th>
				<th>Descripción</th>
				<th>Precio</th>
				<th>Stock</th>
			</tr>
		</tfoot>


	</table>

	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#productosTable_id').DataTable({
				language : {
					url : './resources/js/DataTable_es_ES.json'
				},
				ajax: 
				{
					url: "http://localhost:8080/Ecommerce-jsp-jpa-hibernate/api/productos",
					dataSrc: ""
				},
				columns: [ 
					{data: "id_prod", defaultContent: "No establecido", width: "10%"},
					{data: "nombre_prod", defaultContent: "No establecido", width: "10%"},
					{data: "categoria_prod", defaultContent: "No establecido", width: "10%"},
					{data: "fabricante_prod", defaultContent: "No establecido", width: "10%"},
					{data: "detalle_prod", defaultContent: "No establecido", width: "20%"},
					{data: "descripcion_prod", defaultContent: "No establecido", width: "20%"},
					{data: "precio_prod", defaultContent: "No establecido", width: "10%"},
					{data: "stock_prod", defaultContent: "No establecido", width: "10%"}
				],
				columnDefs: [
    				{
        				targets: '_all',
        				className: 'dt-body-center'
    				}
  				]
			});
		});
	</script>
</body>
</html>