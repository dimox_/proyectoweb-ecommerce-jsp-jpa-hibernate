<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Listado Productos</title>

<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

</head>
<body>
	<h1>LISTADO DE PRODUCTOS</h1>
	<table id="productosTable_id" class="display">
		<thead>
			<tr>
				<th>ID</th>
				<th>NOMBRE</th>
				<th>CATEGORÍA</th>
				<th>FABRICANTE</th>
				<th>DETALLE</th>
				<th>DESCRIPCIÓN</th>
				<th>PRECIO</th>
				<th>STOCK</th>
			</tr>
		</thead>
		<tbody>

			<c:if test="${listadoProductos != null}">
				<c:forEach var="pro" items="${listadoProductos}">
					<tr>
						<td><c:out value="${pro.id_prod}" default="Sin especificar" /></td>
						
						<td><c:out value="${pro.nombre_prod}"
								default="Sin especificar" /></td>
								
						<td><c:out value="${pro.categoria_prod}"
								default="Sin especificar" /></td>
								
						<td><c:out value="${pro.fabricante_prod}"
								default="Sin especificar" /></td>
								
						<td><c:out value="${pro.detalle_prod}"
								default="Sin especificar" /></td>
								
						<td><c:out value="${pro.descripcion_prod}"
								default="Sin especificar" /></td>
								
						<td><c:out value="${pro.precio_prod} €"
								default="Sin especificar" /></td>
								
						<td><c:out value="${pro.stock_prod}"
								default="Sin especificar" /></td>
					</tr>
				</c:forEach>
			</c:if>
		</tbody>
	</table>

	<script src="https://code.jquery.com/jquery-2.2.4.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#productosTable_id').dataTable({
				"language" : {
					url : './resources/js/DataTable_es_ES.json'
				}
			});
		});
	</script>
</body>
</html>