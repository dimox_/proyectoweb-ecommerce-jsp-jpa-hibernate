package com.dimox.ecommerce.tests;

import com.dimox.ecommerce.DAO.IProductoDao;
import com.dimox.ecommerce.DAO.ProductoDaoImplHibernate;
import com.dimox.ecommerce.JPAUtils.JPAUtil;
import com.dimox.ecommerce.helpers.Helpers;
import com.dimox.ecommerce.models.Producto;

public class ProductoTest {

	public static void main(String[] args) throws Exception {
		
		IProductoDao productoDao = new ProductoDaoImplHibernate();
//		ICategoriaDao categoriaDao = new CategoriaDaoImplHibernate();
		
//		ICategoriaDao categoriaDao = new CategoriaDaoImplHibernate();//		
//		IProductoDao productoDao = new ProductoDao();
		
//		Producto producto = new Producto();
//		producto.setNombre_prod("Producto 3");
//		producto.setPrecio_prod(Helpers.getBigTwoDecimals("12.50"));
//		producto.setCategoria_prod(new Categoria("cat1","url"));
//		
//		Producto producto2 = new Producto();
//		producto2.setNombre_prod("Producto 4");
//		producto2.setPrecio_prod(Helpers.getBigTwoDecimals("99.1123"));
//		producto2.setCategoria_prod(new Categoria("cat2","url2"));
//		
//		Producto producto3 = new Producto();
//		producto3.setNombre_prod("Producto 5");
//		producto3.setPrecio_prod(Helpers.getBigTwoDecimals("99.1123"));
//		producto3.setCategoria_prod(new Categoria("cat3","url2"));
//		
//		Producto producto4 = new Producto();
//		producto4.setNombre_prod("Producto 6");
//		producto4.setPrecio_prod(Helpers.getBigTwoDecimals("99.1123"));
//		producto4.setCategoria_prod(new Categoria("cat4","url2"));
//		
//		productoDao.save(producto);
//		productoDao.save(producto2);
//		productoDao.save(producto3);
//		productoDao.save(producto4);
		
		for (int i = 1 ; i <=20 ; i++) {
			productoDao.save(new Producto("Producto"+i, Helpers.getBigTwoDecimals("99.1123")));
			
			
		}
		
		
//		productoDao.update(new Producto(1L,"Siguiente",Helpers.getBigTwoDecimals("12.50"),new Categoria("catNew","urlNew")));
//		productoDao.update(new Producto(2L,"otro",Helpers.getBigTwoDecimals("12.50"),categoria));
//		productoDao.update(new Producto(3L,"otro3",Helpers.getBigTwoDecimals("12.50"),categoria));
		
//		productoDao.delete(4L);
		
//		productoDao.update(producto);
//		productoDao.save(producto);
//		productoDao.delete(2L);
//		productoDao.update(new Producto(3L,"modificado 2", Helpers.getBigTwoDecimals("12.50"),categoria));
		
		JPAUtil.shutdown();
		
	}
}
