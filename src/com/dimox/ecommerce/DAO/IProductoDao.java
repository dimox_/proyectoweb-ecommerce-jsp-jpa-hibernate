package com.dimox.ecommerce.DAO;

import com.dimox.ecommerce.DAO.GenericImpl.IGenericDAO;
import com.dimox.ecommerce.models.Producto;

public interface IProductoDao extends IGenericDAO<Producto, Long> {
	
}
