package com.dimox.ecommerce.DAO;

import java.util.List;

import javax.persistence.Query;

import com.dimox.ecommerce.DAO.GenericImpl.OperacionesDAOImplHibernate;
import com.dimox.ecommerce.models.Producto;
import com.dimox.ecommerce.models.DTO.ProductoCategoriaDTO;

public class ProductoDaoImplHibernate extends OperacionesDAOImplHibernate<Producto, Long> implements IProductoDao{

	private final String CONSULTA = "SELECT DISTINCT new "
			+ "com.dimox.ecommerce.models.DTO.ProductoCategoriaDTO(c.id_prod, c.nombre_prod, c.fabricante_prod, "
			+ "c.detalle_prod, c.descripcion_prod, c.precio_prod, c.stock_prod, c.categoria_prod.nombre_cat) "
			+ "FROM Producto c LEFT JOIN c.categoria_prod";
	
	
	// Traer todos los productos existentes
		@SuppressWarnings("unchecked")
		public List<ProductoCategoriaDTO> findAllProducts() throws Exception {

			try {
				Query query = eManager.createQuery(CONSULTA);
				List<ProductoCategoriaDTO> listEntities = query.getResultList();
				return listEntities;
			} catch (Exception e) {
				if (eManager.getTransaction().isActive()) {
					eManager.getTransaction().rollback();
				}
				throw e;
			} 
		}
	
}
