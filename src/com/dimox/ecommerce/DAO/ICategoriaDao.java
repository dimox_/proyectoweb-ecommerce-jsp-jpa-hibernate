package com.dimox.ecommerce.DAO;

import com.dimox.ecommerce.DAO.GenericImpl.IGenericDAO;
import com.dimox.ecommerce.models.Categoria;

public interface ICategoriaDao extends IGenericDAO<Categoria, Long> {
	
}
