package com.dimox.ecommerce.DAO.GenericImpl;

import java.io.Serializable;
import java.util.List;

public interface IGenericDAO<T, ID extends Serializable> {

	boolean save(T entity) throws Exception;

	boolean update(T entity) throws Exception;

	boolean delete(ID id) throws Exception;

	T findById(ID id) throws Exception;

	List<T> findAll() throws Exception;
}
