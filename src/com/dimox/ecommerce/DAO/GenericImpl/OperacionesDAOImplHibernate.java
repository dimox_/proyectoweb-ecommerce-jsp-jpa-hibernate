package com.dimox.ecommerce.DAO.GenericImpl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.dimox.ecommerce.JPAUtils.JPAUtil;

public class OperacionesDAOImplHibernate<T, ID extends Serializable> implements IGenericDAO<T, ID> {

	protected EntityManager eManager;
	
	public OperacionesDAOImplHibernate() {
		eManager = JPAUtil.getEntityManagerFactory().createEntityManager();
	}

	// Crear producto
	@Override
	public boolean save(T entity) throws Exception {
		boolean saved = true;
		try {
			eManager.getTransaction().begin();
			eManager.persist((eManager.contains(entity)) ? entity : eManager.merge(entity));
			eManager.getTransaction().commit();

		} catch (Exception e) {
			saved = false;
			if (eManager.getTransaction().isActive()) {
				eManager.getTransaction().rollback();
			}
			throw e;
		} 

		return saved;

	}

	// Editar producto
	@Override
	public boolean update(T entity) throws Exception {
		boolean updated = true;
		try {
			eManager.getTransaction().begin();
			T obj = (T) eManager.merge(entity);
			eManager.persist(obj);
			eManager.getTransaction().commit();
		} catch (Exception e) {
			updated = false;
			if (eManager.getTransaction().isActive()) {
				eManager.getTransaction().rollback();
			}
			throw e;
		} 

		return updated;
	}

	// Eliminar un producto
	@Override
	public boolean delete(ID id) throws Exception {
		boolean deleted = true;
		try {
			T entity = (T) eManager.find(getEntityClass(), id);
			if (entity == null) {
				throw new Exception("Los datos a borrar ya no existen");
			}
			eManager.getTransaction().begin();
			eManager.remove(entity);
			eManager.getTransaction().commit();
		} catch (Exception e) {
			deleted = false;
			if (eManager.getTransaction().isActive()) {
				eManager.getTransaction().rollback();
			}
			throw e;
		} 
		return deleted;
	}

	// Buscar un producto por medio de su id
	@Override
	public T findById(ID id) throws Exception {
		try {
			T obj = (T) eManager.find(getEntityClass(), id);
			return obj;
		} catch (Exception e) {
			if (eManager.getTransaction().isActive()) {
				eManager.getTransaction().rollback();
			}
			throw e;
		} 
	}

	// Traer todos los productos existentes
	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll() throws Exception {

		try {
			Query query = eManager.createQuery("SELECT c FROM " +getEntityClass().getName()+ " c");
			List<T> listEntities = query.getResultList();
			return listEntities;
		} catch (Exception e) {
			if (eManager.getTransaction().isActive()) {
				eManager.getTransaction().rollback();
			}
			throw e;
		} 
	}

	@SuppressWarnings({ "unused", "unchecked" })
	private Class<T> getEntityClass() {
		return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

}
