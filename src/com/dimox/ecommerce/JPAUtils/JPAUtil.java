package com.dimox.ecommerce.JPAUtils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
	
	private static final String PERSISTENCE_UNIT = "PERSISTENCE";
	private static EntityManagerFactory emf = null;

	public static EntityManagerFactory getEntityManagerFactory() {

		if (emf == null) {
			emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
		}

		return emf;
	}

	public static void shutdown() {
		if (emf != null)
			emf.close();
	}

}
