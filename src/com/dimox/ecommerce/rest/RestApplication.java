package com.dimox.ecommerce.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("api")
public class RestApplication extends ResourceConfig{

}
