package com.dimox.ecommerce.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.dimox.ecommerce.DAO.ProductoDaoImplHibernate;
import com.dimox.ecommerce.models.DTO.ProductoCategoriaDTO;

@Path("productos")
public class ProductoRestController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<ProductoCategoriaDTO> getAllProducts(){
		ProductoDaoImplHibernate productoDao = new ProductoDaoImplHibernate();
		List<ProductoCategoriaDTO> listProd = null;
		try {
		 listProd =  productoDao.findAllProducts();
		} catch (Exception e) {
			System.out.println("Alg�n problema en getAllProducts()");
			e.printStackTrace();
		}
		return listProd;
	}
}
