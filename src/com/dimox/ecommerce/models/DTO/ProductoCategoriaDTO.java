package com.dimox.ecommerce.models.DTO;

import java.math.BigDecimal;

public class ProductoCategoriaDTO {
	
	private Long id_prod;
	private String nombre_prod;
	private String fabricante_prod;
	private String detalle_prod;
	private String descripcion_prod;
	private BigDecimal precio_prod;
	private int stock_prod;
	private String categoria_prod;
	
	public ProductoCategoriaDTO(Long id_prod, String nombre_prod, String fabricante_prod, String detalle_prod,
			String descripcion_prod, BigDecimal precio_prod, int stock_prod, String categoria_prod) {
		this.id_prod = id_prod;
		this.nombre_prod = nombre_prod;
		this.fabricante_prod = fabricante_prod;
		this.detalle_prod = detalle_prod;
		this.descripcion_prod = descripcion_prod;
		this.precio_prod = precio_prod;
		this.stock_prod = stock_prod;
		this.categoria_prod = categoria_prod;
	}

	public Long getId_prod() {
		return id_prod;
	}

	public void setId_prod(Long id_prod) {
		this.id_prod = id_prod;
	}

	public String getNombre_prod() {
		return nombre_prod;
	}

	public void setNombre_prod(String nombre_prod) {
		this.nombre_prod = nombre_prod;
	}

	public String getFabricante_prod() {
		return fabricante_prod;
	}

	public void setFabricante_prod(String fabricante_prod) {
		this.fabricante_prod = fabricante_prod;
	}

	public String getDetalle_prod() {
		return detalle_prod;
	}

	public void setDetalle_prod(String detalle_prod) {
		this.detalle_prod = detalle_prod;
	}

	public String getDescripcion_prod() {
		return descripcion_prod;
	}

	public void setDescripcion_prod(String descripcion_prod) {
		this.descripcion_prod = descripcion_prod;
	}

	public BigDecimal getPrecio_prod() {
		return precio_prod;
	}

	public void setPrecio_prod(BigDecimal precio_prod) {
		this.precio_prod = precio_prod;
	}

	public int getStock_prod() {
		return stock_prod;
	}

	public void setStock_prod(int stock_prod) {
		this.stock_prod = stock_prod;
	}

	public String getCategoria_prod() {
		return categoria_prod;
	}

	public void setCategoria_prod(String categoria_prod) {
		this.categoria_prod = categoria_prod;
	}
	
}
