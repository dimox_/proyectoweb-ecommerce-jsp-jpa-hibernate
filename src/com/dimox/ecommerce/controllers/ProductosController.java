package com.dimox.ecommerce.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dimox.ecommerce.DAO.ProductoDaoImplHibernate;
import com.dimox.ecommerce.JPAUtils.JPAUtil;
import com.dimox.ecommerce.models.DTO.ProductoCategoriaDTO;

@WebServlet("/listarProductos")
public class ProductosController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public ProductosController() {
    }

	public void destroy() {
		JPAUtil.shutdown();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ProductoDaoImplHibernate productoDao = new ProductoDaoImplHibernate();
		try {
			List<ProductoCategoriaDTO> prodList = productoDao.findAllProducts();
			
			request.setAttribute("listadoProductos", prodList);
			request.getRequestDispatcher("./views/listaProductosAJAX.jsp").forward(request, response);
			
		} catch (Exception e) {
			System.out.println("ERROR EN SERVLET: Productodao_findAll");
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
